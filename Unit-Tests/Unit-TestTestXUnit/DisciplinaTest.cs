#region Using Directives

using System;
using Unit_Tests;
using Unit_Tests.Extensions;
using Xunit;
using Xunit.Abstractions;

#endregion

namespace Unit_TestTestXUnit
{
    public class DisciplinaTest : IDisposable
    {
        private readonly ITestOutputHelper _output;

        public DisciplinaTest(ITestOutputHelper output)
        {
            _output = output;
            output.WriteLine("Construtor sendo executado");
        }

        public void Dispose()
        {
            _output.WriteLine("Dispose sendo executado");
        }

        [Theory (DisplayName = "SeDisciplinaInicializarComStringEmptyOuNull")]
        [InlineData("")]
        [InlineData(null)]

        public void SeDisciplinaInicializarComStringEmptyOuNull(string nome)
        {
           var _expected = "O parametro n�o pode ser null (Parameter 'Nome')";

           Assert.Throws<ArgumentException>(() => new Disciplina(1, nome)).AssertMessage(_expected);
        }
    }
}
