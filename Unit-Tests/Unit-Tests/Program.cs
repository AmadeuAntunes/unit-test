﻿using DataBase;
using System;

namespace Unit_Tests
{
    class Program
    {
        static void Main(string[] args)
        {
            var _escola = new Escola("Escola básica 2, 3 das Amoreiras");
            _escola.AdicionarPessoa(new Director(1, "Mário"));

            var _professor = new Professor(2, "Alberto");
            var _curso = new Curso(1, "Curso Ciências");
            var _turna = new Turma("A");


            _turna.AdicionarAluno(new Aluno(1, "Amadeu"));
            _turna.AdicionarAluno(DbInfo.AddItem(typeof(Aluno), 2, "Maria") as Aluno);
            _curso.AdicionarTurma(new Turma("A"));


            _curso.AdicionaDisciplina(new Disciplina(1, "Matemática"));
            _escola.AdicionarCurso(_curso);


            foreach (var _cursoItem in _escola.Cursos)
            {
                Console.WriteLine($"Nome do Curso:{_cursoItem.Nome}");

                foreach (var _turmaItem in _cursoItem.Turmas)
                {
                    Console.WriteLine($"{_turmaItem.Nome}");

                    foreach (var _alunoItem in _turmaItem.Alunos)
                    {
                        Console.WriteLine(_alunoItem.Nome);
                    }
                }

                foreach (var _disciplinaItem in _cursoItem.Disciplinas)
                {
                    Console.WriteLine($"{_disciplinaItem.Nome}");
                }
            }
            Console.ReadKey();
        }
    }
}
