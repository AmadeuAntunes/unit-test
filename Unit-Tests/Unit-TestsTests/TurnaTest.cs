﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Unit_Tests;
using Unit_TestsTests.Builder;

namespace Unit_TestsTests
{
    [TestClass]
    public class TurnaTest
    {

        [TestMethod]
        [Owner("Amadeu Antunes")]
        [TestCategory("Exception")]
        [ExpectedException(typeof(ArgumentException), "Não ocorreu excepção")]
        public void SeAdicionarAlunoNull()
        {

            Turma _turma = TurmaBuilder.Novo().Build();
            Aluno _aluno = null;
                
            _turma.AdicionarAluno(_aluno);
        }

 
        [TestMethod]
        [Owner("Amadeu Antunes")]
        [TestCategory("Exception")]
        [ExpectedException(typeof(ArgumentException), "Não ocorreu excepção")]
        public void SeAdicionarClassificacaoNull()
        {
            Turma _turma = TurmaBuilder.Novo().Build();
            _turma.AdicionarClassificação(null);

        }

    }

}
