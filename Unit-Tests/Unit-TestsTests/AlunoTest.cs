﻿using Bogus;
using DataBase;
using DataBase.Contractos;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using Unit_Tests;
using Unit_TestsTests.Builder;

namespace Unit_TestsTests
{
    [TestClass]
    public class AlunoTest
    {
        [TestMethod]
        public void AdicionarCurso()
        {
            var _repositorio = new Mock<IRepositorio>();

            Repositorio _ConcreteRepositorio = new Repositorio(_repositorio.Object);

            var _aluno = AlunoBuider.Novo()
                .ComNome(new Faker().Name.FirstName())
                .ComId(new Faker().Random.Int())
                .Build();

            _ConcreteRepositorio.Guardar(_aluno);


            _repositorio.Verify(x => x.Guardar(It.Is<Aluno>(x => x.Id == _aluno.Id)));
            _repositorio.VerifyNoOtherCalls();
            

        }

    }
}
