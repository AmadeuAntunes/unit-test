﻿using System;
using System.Collections.Generic;
using System.Text;
using Unit_Tests;

namespace Unit_TestsTests.Builder
{
    public class TurmaBuilder
    {
        public int Id { get; set; } = 1;
        public string Nome { get; set; } = "Turma A";
        public List<Aluno> Alunos { get; set; } = new List<Aluno>();
        public List<Classificacao> ListaClassificacao { get; set; } = new List<Classificacao>();


        public TurmaBuilder ComListaClassificacao(List<Classificacao> ListaClassificacao)
        {
            ListaClassificacao = ListaClassificacao;
            return this;
        }


        public TurmaBuilder ComAlunos(List<Aluno> alunos)
        {
            Alunos = alunos;
            return this;
        }

        public TurmaBuilder ComNome(string nome)
        {
            Nome = nome;
            return this;
        }

        public TurmaBuilder ComId(int id)
        {
            Id = id;
            return this;
        }

        public static TurmaBuilder Novo()
        {
            return new TurmaBuilder();
        }

        public Turma Build()
        {
            return new Turma(Nome);
        }
    }
}
