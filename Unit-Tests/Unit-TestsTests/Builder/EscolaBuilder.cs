﻿using Unit_Tests;

namespace Unit_TestsTests.Builder
{
    public class EscolaBuilder
    {
        private string _nome = "aaaa";
        public static EscolaBuilder Novo()
        {
            return new EscolaBuilder();
        }

        public EscolaBuilder ComNome(string nome)
        {
            _nome = nome;
            return this;
        }

        public Escola Build()
        {
            return new Escola(_nome);
        }
    }
}
