﻿using DevExpress.Xpo;
using Unit_Tests;

namespace Unit_TestsTests.Builder
{

    public class AlunoBuider 
    {
        private string Nome { get; set; } = "aaaa";
        public int Id { get; set; } = 1;


        public static AlunoBuider Novo()
        {
            return new AlunoBuider();
        }
        public AlunoBuider ComNome(string nome)
        {
            Nome = nome;

            return this;
        }
        public AlunoBuider ComId(int id)
        {
            Id = id;

            return this;
        }


        public Aluno Build()
        {
            return new Aluno(Id, Nome);
        }
    }

}