﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Unit_Tests;

namespace Unit_TestsTests
{
    [TestClass]
    public class DisciplinaTest
    {
        [TestMethod]
        [Owner("Amadeu Antunes")]
        [TestCategory("Exception")]
        [ExpectedException(typeof(ArgumentException))]
        public void SeDisciplinaInicializarComNull()
        {
            Disciplina _disciplina = new Disciplina(1, null);
        }


        [TestMethod]
        [Owner("Amadeu Antunes")]
        [TestCategory("Exception")]
        [ExpectedException(typeof(ArgumentException))]
        public void SeDisciplinaInicializarComStringEmpty()
        {
            string _nome = string.Empty;

            Disciplina _disciplina = new Disciplina(1 , _nome);
            
        }
    }
}
