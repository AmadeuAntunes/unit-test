#region Using Directives
using Bogus;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Unit_Tests;
using Unit_TestsTests.Builder;
#endregion

namespace Unit_TestsTests
{
    [TestClass]
    public class EscolaTest
    {
        #region Instacias
        [TestMethod]
        [Owner("Amadeu Antunes")]
        [TestCategory("Exception")]
        [ExpectedException(typeof(ArgumentException))]
        [Description("Lan�a Exce��o se o nome da escola tem menos de uma letra")]
        public void SeNomeDaEscolaTemApenasUmaLetra()
        {
            Escola _escola = new Escola("A");
           
        }

        [TestMethod]
        [Owner("Amadeu Antunes")]
        [TestCategory("Exception")]
        [ExpectedException(typeof(ArgumentException))]
        public void SeCriarAEscolaComNomeNull()
        {
            Escola _escola = new Escola(null);
        }


        [TestMethod]
        [Owner("Amadeu Antunes")]
        [TestCategory("Instance")]

        public void SeRetornaInstanciaDirector()
        {
            Escola _escola = new Escola("Teste");
            Pessoa _pessoa = new Director(1 ,"D�rio");


            Assert.IsInstanceOfType(_pessoa, typeof(Director));
        }

        #endregion

        #region AdicionarPessoa

        [TestMethod]
        [Owner("Amadeu Antunes")]
        [TestCategory("Exception")]
        [ExpectedException(typeof(ArgumentException))]
        public void VerificarSePossivelInserirIDDuplicados()
        {
            Escola _escola = EscolaBuilder.Novo().ComNome("Teste").Build();

            Aluno _aluno1 = new Aluno(1, new Faker().Name.FirstName());
            Aluno _aluno2 = new Aluno(1, new Faker().Name.FirstName());
    

            _escola.AdicionarPessoa(_aluno1);
           
            _escola.AdicionarPessoa(_aluno2);
        }

        [TestMethod]
        [Owner("Amadeu Antunes")]
        [TestCategory("Exception")]
        [ExpectedException(typeof(ArgumentException))]
        public void SeAdicionarPessoaNullTest()
        {
            Escola _escola = new Escola("Escola test");

            _escola.AdicionarPessoa(null);
        }

        #endregion

        #region AdicionarCurso
        [TestMethod]
        [Owner("Amadeu Antunes")]
        [TestCategory("Exception")]
        [ExpectedException(typeof(ArgumentException))]
        public void SeAdicionarCursoNull()
        {
            Escola _escola = new Escola("Test");

            _escola.AdicionarCurso(null);

        }

        #endregion

        #region AdicionaDisciplina
        [TestMethod]
        [Owner("Amadeu Antunes")]
        [TestCategory("Exception")]
        [ExpectedException(typeof(ArgumentException), "N�o ocorreu excep��o")]
        public void SeAdicionarCursoStringEmpty()
        {
            Escola _escola = new Escola("Test");

            Curso _curso = new Curso(0, "Matem�tica");
            _curso.AdicionaDisciplina(null);
        }



        #endregion
    }
}
