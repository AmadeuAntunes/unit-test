﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Unit_Tests;

namespace Unit_TestsTests
{
    [TestClass]
    public class ProfessorTest
    {
        [Owner("Amadeu Antunes")]
        [TestCategory("Exceptions")]
        [TestMethod]
        public void AdicionarDisciplinaNullTest()
        {
            Professor _professor = new Professor(1, "António");
            Disciplina _disciplina = null;

            Assert.ThrowsException<ArgumentException>(() => _professor.AdicionarDisciplina(_disciplina));
        }


        [Owner("Amadeu Antunes")]
        [TestCategory("Exceptions")]
        [ExpectedException(typeof(ArgumentException))]
        [TestMethod]
        public void AdicionarduasDisciplinaComMesmaInstanciaTest()
        {
            Professor _professor = new Professor(1, "António");
            Disciplina disciplina = new Disciplina(1, "Matemática");

            _professor.AdicionarDisciplina(disciplina);
            _professor.AdicionarDisciplina(disciplina);
         
        }


        [Owner("Amadeu Antunes")]
        [TestCategory("Exceptions")]
        [ExpectedException(typeof(ArgumentException))]
        [TestMethod]
        public void AdicionarduasDisciplinasComIgualValorTestt()
        {
            Professor _professor = new Professor(1, "António");
            Disciplina _disciplina1 = new Disciplina(1,"Matemática");
            Disciplina _disciplina2 = new Disciplina(1, "Matemática");

            _professor.AdicionarDisciplina(_disciplina1);
            _professor.AdicionarDisciplina(_disciplina2);
           
        }
    }
   
}
