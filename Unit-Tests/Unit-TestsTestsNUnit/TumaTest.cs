﻿using NUnit.Framework;
using System;
using Unit_Tests;

namespace Unit_TestsTestsNUnit
{
    public class TumaTest
    {
        [Test]
        public void SeAdicionarAlunoNull()
        {
            Turma _turma = new Turma("A");
            Aluno _aluno = null;

            var _mensagem = Assert.Throws<ArgumentException>(() => _turma.AdicionarAluno(_aluno)).Message;
            StringAssert.AreNotEqualIgnoringCase("O parametro não pode ser null", _mensagem);
        }
    }
}
