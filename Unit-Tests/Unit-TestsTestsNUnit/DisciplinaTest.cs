using NUnit.Framework;
using System;
using Unit_Tests;
using Unit_Tests.Extensions;

namespace Unit_TestsTestsNUnit
{
    public class DisciplinaTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void SeDisciplinaInicializarComNull()
        {
            Assert.Throws<ArgumentException>(() => new Disciplina(1, null));
        }

        [Test]
        [Timeout(1000)]
        public void SeDisciplinaInicializarComStringEmpty()
        {
            string _nome = string.Empty;
            var _expected = @"O parametro n�o pode ser null (Parameter 'Nome')";

            Assert.Throws<ArgumentException>(() => new Disciplina(1, _nome)).AssertMessage(_expected);
        }
    }
}