﻿using NUnit.Framework;
using System;
using Unit_Tests;
using Unit_Tests.Extensions;

namespace Unit_TestsTestsNUnit
{
    public class ProfessorTest
    {
        [Test]
        public void AdicionarDisciplinaNullTest()
        {
            Professor _professor = new Professor(1, "António");
            Disciplina _disciplina = null;

            var _expected = "O parametro não pode ser null (Parameter 'disciplina')";

            Assert.Throws<ArgumentException>(() => _professor.AdicionarDisciplina(_disciplina))
                  .AssertMessage(_expected);

        }
    }
}
