﻿using NUnit.Framework;
using System;
using Unit_Tests;
using Unit_Tests.Extensions;

namespace Unit_TestsTestsNUnit
{
    public class EscolaTest
    {

        [Test(Author = "Amadeu Antunes")]
        public void SeNomeDaEscolaTemApenasUmaLetra()
        {
           var _expected = @"O parametro não pode ter menos de 3 letras (Parameter 'nome')";

           Assert.Throws<ArgumentException>(() => new Escola("A")).AssertMessage(_expected);
        }


        [Test(Author = "Amadeu Antunes")]
        public void SeCriarAEscolaComNomeNull()
        {
    
            var _expected = @"O parametro não pode ser null (Parameter 'nome')";

            Assert.Throws<ArgumentException>(() => new Escola(null)).AssertMessage(_expected);
        }
    }
}
