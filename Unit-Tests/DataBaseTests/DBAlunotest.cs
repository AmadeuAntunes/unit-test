using Bogus;
using DataBase;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Unit_Tests;

namespace DataBaseTests
{
    [TestClass]
    public class DBAlunotest
    {
        [TestMethod]
        public void VerificaSEDbAlunoTemListaNull()
        {
            var _dbAluno = new DbInfo();

            Assert.IsNotNull(_dbAluno.Tables);
        }

        [TestMethod]
        public void VerificarSeAdicionaAluno()
        {
            var _dbAluno = new DbInfo();
            var _aluno = new Aluno(new Faker().Random.Number(), new Faker().Name.FirstName());

            _dbAluno.Guardar(_aluno);

            var _expected = _dbAluno.Tables.Count > 0;
            Assert.IsTrue(_expected);
            
        }
       
    }
}
