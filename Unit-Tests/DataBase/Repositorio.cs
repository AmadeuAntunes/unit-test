﻿using DataBase.Contractos;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataBase
{
    public class Repositorio 
    {
        readonly IRepositorio Repos;
        public Repositorio(IRepositorio repos)
        {
            this.Repos = repos;
        }

        public void ApagarTodaALista() 
            => Repos.ApagarTodaALista();


        public void ApagarUmItem(ITabelaBase tabela) 
            => Repos.ApagarUmItem(tabela);


        public IList<ITabelaBase> Guardar(ITabelaBase tabela) 
            =>  Repos.Guardar(tabela);

    }
}
