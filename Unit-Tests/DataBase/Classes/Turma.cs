﻿using System;
using System.Collections.Generic;

namespace Unit_Tests
{
    public class Turma 
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public List<Aluno> Alunos { get; set; }

        public List<Classificacao> ListaClassificacao { get; set; }
        public Turma(string nome)
        {
            if(string.IsNullOrEmpty(nome))
                throw new ArgumentException("O parametro não pode ser null", "nome");

            Alunos = new List<Aluno>();
            ListaClassificacao = new List<Classificacao>();
            Nome = nome;
        }

        public void AdicionarAluno(Aluno aluno)
        {
            if(aluno == null)
                throw new ArgumentException("O parametro não pode ser null", "aluno");
            Alunos.Add(aluno);
        }

        public void AdicionarClassificação(Classificacao classificacao)
        {
            if(classificacao == null)
                throw new ArgumentException("O parametro não pode ser null", "classificacao");
            ListaClassificacao.Add(classificacao);
        }
    }
}
