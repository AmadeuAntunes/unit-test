﻿namespace Unit_Tests
{
    public class Pessoa :IPessoa
    {
        public int Id { get; set; }
        public string Nome { get; set; }

        public Pessoa(int id, string nome)
        {
            this.Id = id;
            this.Nome = nome;
        }  
    }
}
