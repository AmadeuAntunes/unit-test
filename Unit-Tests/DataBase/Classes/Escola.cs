﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Unit_Tests
{
    public class Escola
    {
        public string Nome { get; set; }
        public List<Pessoa> Pessoas { get; set; }
        public List<Curso> Cursos { get; set; }


        public Escola(string nome)
        {
            if (nome == null)
                throw new ArgumentException("O parametro não pode ser null", "nome");

            if (nome.Length < 3)
                throw new ArgumentException("O parametro não pode ter menos de 3 letras", "nome");
            
            Pessoas = new List<Pessoa>();
            Cursos = new List<Curso>();

            this.Nome = nome;     
        }
        public void AdicionarPessoa(Pessoa pessoa)
        {
            if (pessoa == null)
                throw new ArgumentException("O parametro não pode ser null", "pessoa");

            if(EDuplicado(pessoa))
                throw new ArgumentException("O Id da pessoa é duplicado", "pessoa");

            Pessoas.Add(pessoa);
        }

        public void AdicionarCurso(Curso curso)
        {

            if (curso == null)
                throw new ArgumentException("O parametro não pode ser null", "curso");
            Cursos.Add(curso);

        }

        public bool EDuplicado(Pessoa pessoa)
        {
            return Pessoas.Where(x => x.Id == pessoa.Id).Any();
        }
    }
}
