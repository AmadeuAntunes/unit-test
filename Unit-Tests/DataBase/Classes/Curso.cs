﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Unit_Tests
{
    public class Curso : ICurso
    {
        public List<Disciplina> Disciplinas { get; set; }
        public List<Turma> Turmas { get; set; }
        public int Id { get; set; }
        public string Nome { get; set; }

        public Curso(int id, string nome)
        {
            Disciplinas = new List<Disciplina>();
            Turmas = new List<Turma>();
            this.Nome = nome;
            this.Id = id;
        }

        public void AdicionaDisciplina(Disciplina disciplina)
        {
            if (disciplina == null)
                throw new ArgumentException("O parametro não pode ser null", "disciplina");
            Disciplinas.Add(disciplina);
        }

        public void AdicionarTurma(Turma turma)
        {
            if (turma == null)
                throw new ArgumentException("O parametro não pode ser null", "turma");
            Turmas.Add(turma);
        }
    }
}
