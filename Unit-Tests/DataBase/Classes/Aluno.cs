﻿using DataBase;
using System;
using System.Collections.Generic;
using System.Text;

namespace Unit_Tests
{
    public class Aluno : Pessoa, IAluno
    {
        public int MediaFinal { get; set; }
        public Aluno(int id, string nome) : base(id, nome)
        {

        }
    }
}
