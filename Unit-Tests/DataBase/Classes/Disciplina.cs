﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Unit_Tests
{
    public class Disciplina : IDisciplina
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public Disciplina(int id, string Nome)
        {
            this.Id = id;
            this.Nome = Nome;

            if (string.IsNullOrEmpty(Nome))
                throw new ArgumentException("O parametro não pode ser null", "Nome");
        }

    }

}
