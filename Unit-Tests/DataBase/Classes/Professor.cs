﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Unit_Tests
{
    public class Professor : Pessoa
    {
        public readonly List<Disciplina> DisciplinasLecionadas = new List<Disciplina>();
        public Professor(int id, string nome) : base(id, nome)
        {
            DisciplinasLecionadas = new List<Disciplina>();
        }

        public void AdicionarDisciplina(Disciplina disciplina)
        {
            if (disciplina == null)
                throw new ArgumentException("O parametro não pode ser null", "disciplina");

            if(DisciplinasLecionadas.Contains(disciplina))
                throw new ArgumentException("Não é possivel adicionar objects com a mesma instancia na lista", "disciplina");
            if(DisciplinasLecionadas.Where( x => x.Id == disciplina.Id && x.Nome == disciplina.Nome).Any())
                throw new ArgumentException("Não é possivel adicionar disciplinas com igual valor", "disciplina");


            DisciplinasLecionadas.Add(disciplina);
        }
    }
}
