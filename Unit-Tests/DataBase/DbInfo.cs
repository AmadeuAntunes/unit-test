﻿using DataBase.Contractos;
using System;
using System.Collections.Generic;
using Unit_Tests;

namespace DataBase
{
    public class DbInfo : IRepositorio
    {
        public List<ITabelaBase> Tables { get; }
        public DbInfo()
        {
            Tables = new List<ITabelaBase>();
        }
        public IList<ITabelaBase> Guardar(ITabelaBase tabela)
        {
             Tables.Add(tabela);
              return Tables;
        }
           


        public void ApagarTodaALista()
            => Tables.Clear();

        public void ApagarUmItem(ITabelaBase tabela)
        {
            if (Tables.Contains(tabela))
                Tables.Remove(tabela);
        }

        public static ITabelaBase AddItem(Type type, int id, string nome)
        {
            if (type == typeof(Aluno))
                return new Aluno(id, nome);

            return null;
        }
    }
}
