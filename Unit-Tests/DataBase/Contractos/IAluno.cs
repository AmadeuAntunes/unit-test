﻿using DataBase.Contractos;

namespace Unit_Tests
{
    public interface IAluno : ITabelaBase
    {
        int MediaFinal { get; set; }
    }
}