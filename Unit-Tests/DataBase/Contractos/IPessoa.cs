﻿using DataBase.Contractos;

namespace Unit_Tests
{
    public interface IPessoa : ITabelaBase
    {
         int Id { get; set; }
         string Nome { get; set; }
    }
}