﻿using DataBase.Contractos;
using System.Collections.Generic;

namespace Unit_Tests
{
    public interface ICurso : ITabelaBase
    {
        List<Disciplina> Disciplinas { get; set; }
        int Id { get; set; }
        string Nome { get; set; }
        List<Turma> Turmas { get; set; }

        void AdicionaDisciplina(Disciplina disciplina);
        void AdicionarTurma(Turma turma);
    }
}