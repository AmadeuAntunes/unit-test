﻿using DataBase.Contractos;

namespace Unit_Tests
{
    public interface IDisciplina : ITabelaBase
    {
        int Id { get; set; }
        string Nome { get; set; }
    }
}