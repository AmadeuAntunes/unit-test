﻿using DataBase.Contractos;
using System.Collections.Generic;

namespace DataBase
{
    public interface IRepositorio
    {
        IList<ITabelaBase> Guardar(ITabelaBase tabela);
        void ApagarTodaALista();
        void ApagarUmItem(ITabelaBase tabela);
    }
}