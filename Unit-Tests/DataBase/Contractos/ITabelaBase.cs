﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataBase.Contractos
{
    public interface ITabelaBase
    {
        int Id { get; set; }
        string Nome { get; set; }
    }
}
